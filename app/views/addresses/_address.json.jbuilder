json.extract! address, :id, :country, :city, :state, :street, :number, :student_id, :professor_id, :user_id, :created_at, :updated_at
json.url address_url(address, format: :json)
