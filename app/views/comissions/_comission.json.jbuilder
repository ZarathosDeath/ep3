json.extract! comission, :id, :sale_id, :value, :professor_id, :status, :notes, :created_at, :updated_at
json.url comission_url(comission, format: :json)
