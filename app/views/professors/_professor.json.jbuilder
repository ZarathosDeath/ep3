json.extract! professor, :id, :name, :email, :role_id, :status, :notes, :balance, :created_at, :updated_at
json.url professor_url(professor, format: :json)
