json.extract! discount, :id, :name, :description, :value, :kind, :status, :created_at, :updated_at
json.url discount_url(discount, format: :json)
