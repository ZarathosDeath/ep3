json.extract! sale, :id, :student_id, :sale_date, :professor_id, :discount_id, :notes, :created_at, :updated_at
json.url sale_url(sale, format: :json)
