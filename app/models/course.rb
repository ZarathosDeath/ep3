class Course < ApplicationRecord
    enum status: [:active, :inactive]
    has_many :professor
    mount_uploader :photo, PhotoUploader
end
