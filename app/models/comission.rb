class Comission < ApplicationRecord
  belongs_to :sale
  belongs_to :professor
  enum status: [:pending, :payd]
end
