class Professor < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
    enum kind: [:professor, :system]
    enum status: [:active, :inactive]
    has_many :comissions
    has_many :addresses
    has_many :students
    has_many :courses
    has_many :sales
    mount_uploader :photo, PhotoUploader
end
