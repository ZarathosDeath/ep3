class Sale < ApplicationRecord
  belongs_to :student
  belongs_to :professor
  belongs_to :discount
  has_many :courses
  has_one :comission

  after_save do
    calc = 0
    self.courses.each {|c| calc += c.price}

    if self.discount
      if self.discount.kind == "percent"
        calc -= calc / self.discount.value
      elsif self.discount.kind == "money"
        calc -= self.discount.value
      end
    end
 
    if self.comission.present?
      self.comission.update(value: (calc * 0.15), status: :pending)
    else
      Comission.create(value: (calc * 0.15), professor: self.professor, sale: self, status: :pending)
    end
  end

end
