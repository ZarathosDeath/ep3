class Ability
  include CanCan::Ability

  def initialize(user)
    if user
      if user.role_id == 'system'
        can :manage, :all
      elsif
        can :manage, :all
      end
    end
  end
end
