class Student < ApplicationRecord
    enum status: [:active, :inactive]
    has_one :address
    mount_uploader :photo, PhotoUploader
end
