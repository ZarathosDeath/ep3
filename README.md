# README
* Ruby version - 2.5.3

* System dependencies:
   <ul>
    <li>Ruby</li>
    <li>Rails</li>
    </ul>

* Configuration:
 <ul>
    <ul>Clone repository, and inside local repository folder:
        <li>$bundle install</li>
        <li>$rails db:migrate</li>
    </ul>
    <ul>then:
        <li>$rails s</li>
    </ul>
 </ul>
* Database creation
        <ul><li>$rails db:reset</li></ul>

* Database initialization
        <ul><li>$rails db:seed</li></ul>
