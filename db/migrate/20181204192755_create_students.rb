class CreateStudents < ActiveRecord::Migration[5.0]
  def change
    create_table :students do |t|
      t.string :name
      t.string :phone
      t.text :notes
      t.integer :status
      t.string :email

      t.timestamps
    end
  end
end
