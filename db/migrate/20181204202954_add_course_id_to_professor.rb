class AddCourseIdToProfessor < ActiveRecord::Migration[5.0]
  def change
    add_column :professors, :course_id, :integer
  end
end
