class CreateComissions < ActiveRecord::Migration[5.0]
  def change
    create_table :comissions do |t|
      t.references :sale, foreign_key: true
      t.decimal :value
      t.references :professor, foreign_key: true
      t.integer :status
      t.text :notes

      t.timestamps
    end
  end
end
