class AddSaleIdToCourse < ActiveRecord::Migration[5.0]
  def change
    add_column :courses, :sale_id, :integer
  end
end
