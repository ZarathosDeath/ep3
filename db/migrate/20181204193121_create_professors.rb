class CreateProfessors < ActiveRecord::Migration[5.0]
  def change
    create_table :professors do |t|
      t.string :name
      t.string :email
      t.integer :role_id
      t.integer :status
      t.text :notes
      t.decimal :balance

      t.timestamps
    end
  end
end
