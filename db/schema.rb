# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20181204204717) do

  create_table "addresses", force: :cascade do |t|
    t.string   "country"
    t.string   "city"
    t.string   "state"
    t.string   "street"
    t.string   "number"
    t.integer  "student_id"
    t.integer  "professor_id"
    t.integer  "user_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["professor_id"], name: "index_addresses_on_professor_id"
    t.index ["student_id"], name: "index_addresses_on_student_id"
  end

  create_table "comissions", force: :cascade do |t|
    t.integer  "sale_id"
    t.decimal  "value"
    t.integer  "professor_id"
    t.integer  "status"
    t.text     "notes"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["professor_id"], name: "index_comissions_on_professor_id"
    t.index ["sale_id"], name: "index_comissions_on_sale_id"
  end

  create_table "courses", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.integer  "status"
    t.string   "category"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.decimal  "price"
    t.integer  "sale_id"
    t.string   "photo"
  end

  create_table "discounts", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "value"
    t.integer  "kind"
    t.integer  "status"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "professors", force: :cascade do |t|
    t.string   "name"
    t.integer  "role_id"
    t.integer  "status"
    t.text     "notes"
    t.decimal  "balance"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "course_id"
    t.string   "photo"
    t.index ["email"], name: "index_professors_on_email", unique: true
    t.index ["reset_password_token"], name: "index_professors_on_reset_password_token", unique: true
  end

  create_table "sales", force: :cascade do |t|
    t.integer  "student_id"
    t.date     "sale_date"
    t.integer  "professor_id"
    t.integer  "discount_id"
    t.text     "notes"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["discount_id"], name: "index_sales_on_discount_id"
    t.index ["professor_id"], name: "index_sales_on_professor_id"
    t.index ["student_id"], name: "index_sales_on_student_id"
  end

  create_table "students", force: :cascade do |t|
    t.string   "name"
    t.string   "phone"
    t.text     "notes"
    t.integer  "status"
    t.string   "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "photo"
  end

end
