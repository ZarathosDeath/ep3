#Students
Student.create name: 'João', status: :active, phone: '123456', notes: 'First Student' ,email: 'student@test.com'
Student.create name: 'Maria', status: :active, phone: '55552323', notes: 'Second Student' ,email: 'student2@test.com'
Student.create name: 'Joaquim', status: :active, phone: '63532212', notes: 'Third Student' ,email: 'student3@test.com'

#System and professors
Professor.create name: 'System', status: :active, role_id: 'system', email: 'system@test.com', password: '123456'
Professor.create name: 'Tom', status: :active, role_id: 'professor', email: 'professor@test.com', password: '123456'
Professor.create name: 'Luis', status: :active, role_id: 'professor', email: 'professor2@test.com', password: '123456'

#Courses
Course.create name: 'Curso de JavaScript', status: :active, category: 'Desenvolvimento', description: 'Curso básico de JavaScript', price: 15
Course.create name: 'Curso de C/C++', status: :active, category: 'Desenvolvimento', description: 'Curso completo de C/C++', price: 20
Course.create name: 'Curso de Ruby on Rails', status: :active, category: 'Desenvolvimento', description: 'Curso completo Ruby on Rails', price: 25

#Descontos
Discount.create name: 'Desconto de Black-Friday', status: :active, value: '10', kind: 'percent', description: 'Desconto de Black-Friday'