require 'test_helper'

class ComissionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @comission = comissions(:one)
  end

  test "should get index" do
    get comissions_url
    assert_response :success
  end

  test "should get new" do
    get new_comission_url
    assert_response :success
  end

  test "should create comission" do
    assert_difference('Comission.count') do
      post comissions_url, params: { comission: { notes: @comission.notes, professor_id: @comission.professor_id, sale_id: @comission.sale_id, status: @comission.status, value: @comission.value } }
    end

    assert_redirected_to comission_url(Comission.last)
  end

  test "should show comission" do
    get comission_url(@comission)
    assert_response :success
  end

  test "should get edit" do
    get edit_comission_url(@comission)
    assert_response :success
  end

  test "should update comission" do
    patch comission_url(@comission), params: { comission: { notes: @comission.notes, professor_id: @comission.professor_id, sale_id: @comission.sale_id, status: @comission.status, value: @comission.value } }
    assert_redirected_to comission_url(@comission)
  end

  test "should destroy comission" do
    assert_difference('Comission.count', -1) do
      delete comission_url(@comission)
    end

    assert_redirected_to comissions_url
  end
end
