Rails.application.routes.draw do
  get 'session/new'
  get 'pages/about'
  get 'pages/home'
  root "pages#home"
  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'

  
  devise_for :professors
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  resources :comissions
  resources :sales
  resources :discounts
  resources :courses
  resources :addresses
  resources :professors
  resources :students

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
