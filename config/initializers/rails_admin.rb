RailsAdmin.config do |config|

  config.main_app_name = ["Plataforma de Cursos Online", "Admin"]


  ### Popular gems integration

  ## == Devise ==
   config.authenticate_with do
    warden.authenticate! scope: :professor
   end
   config.current_user_method(&:current_professor)

  ## == Cancan ==
   config.authorize_with :cancan

  ## == Pundit ==
  # config.authorize_with :pundit

  ## == PaperTrail ==
  # config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0

  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

  ## == Gravatar integration ==
  ## To disable Gravatar integration in Navigation Bar set to false
  # config.show_gravatar = true

   config.model Sale do
      create do
        field :student
        field :sale_date
        field :discount
        field :notes
        field :courses

        field :professor_id, :hidden do
          default_value do
            bindings[:view]._current_user.id
          end
        end
      end

      edit do
        field :client
        field :sale_date
        field :discount
        field :notes
        field :courses
      end
    end

    config.model Student do
      create do
        field :name
        field :email
        field :phone
        field :notes
        field :status
        field :address
        field :photo
      end

      edit do
        field :name
        field :email
        field :phone
        field :notes
        field :status
        field :address
        field :photo
      end

      list do
        field :name
        field :email
        field :phone
        field :status
      end
    end

    config.model Address do
      visible false
    end

    config.model Course do
      create do
        field :name
        field :description
        field :status
        field :category
        field :price
        field :professor
        field :photo
      end

      edit do
        field :name
        field :description
        field :status
        field :category
        field :price
        field :professor
        field :photo
      end
    end

    config.model Professor do
      create do
        field :name
        field :status
        field :email
        field :password
        field :notes
        field :addresses
        field :photo
      end

      edit do
        field :name
        field :status
        field :email
        field :password
        field :notes
        field :addresses
        field :courses
        field :photo
      end
    end

    config.model Discount do
      create do
        field :name
        field :description
        field :value
        field :kind
        field :status
      end
    end

    config.model Discount do
      parent Course
    end

    config.model Comission do
      parent Professor
      weight -1
    end

    config.model Sale do
      parent Professor
      weight -2
    end



  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new
    export
    bulk_delete
    show
    edit
    delete
    show_in_app

    ## With an audit adapter, you can add:
    # history_index
    # history_show
  end
end
